//Utility methods

Array.prototype.forEach = function(callback){
    for (var i = 0; i < this.length; ++i) {
        callback(this[i], i);
    }
};

Array.prototype.filter = function(callback){
    var result = [];
    for (var i = 0; i < this.length; ++i) {
        if (callback(this[i], i)) {
            result.push(this[i]);
        }
    }
    return result;
};

Array.prototype.some = function(callback){
    for (var i = 0; i < this.length; ++i) {
        if (callback(this[i], i)) {
            return true;
        }
    }
    return false;
};

//Watermarker class
var Watermarker = function(){
    function Watermarker() {
        this.init();
    }
    Watermarker.prototype.run = function(){
        var folder = Folder.selectDialog('Выберите папку с изображениями для наложения водяного знака');
        if (folder == null) {
            return false;
        }
        //app.displayDialogs = DialogModes.ALL;

        this.setPreferences();
        this.processDir(folder);
        this.restorePreferences();
    };
    Watermarker.prototype.init = function(){
        this.hasJpegFiles = false;
        this.jpegQuality = 12;
    };
    Watermarker.prototype.setPreferences = function(){
        this.startRulerUnits = app.preferences.rulerUnits;
        this.startTypeUnits = app.preferences.typeUnits;
        this.startDisplayDialogs = app.displayDialogs;

        app.preferences.rulerUnits = Units.INCHES;
        app.preferences.typeUnits = TypeUnits.POINTS;
        app.displayDialogs = DialogModes.NO;
    };
    Watermarker.prototype.restorePreferences = function(){
        app.preferences.rulerUnits = this.startRulerUnits;
        app.preferences.typeUnits = this.startTypeUnits;
        app.displayDialogs = this.startDisplayDialogs;
    };
    Watermarker.prototype.processDir = function(root){
        var that = this,
            fileList = root.getFiles(),
            folders = [],
            filesCount = 0,
            folder,
            count;

        for (var i = 0; i < fileList.length; i++) {
            if (fileList[i] instanceof Folder) {
                count = fileList[i].getFiles().length;
                if (!count) {
                    continue;
                }
                filesCount += count;
                folders.push(fileList[i]);
                //Look for jpegs
                if (!that.hasJpegFiles) {
                    that.hasJpegFiles = fileList[i].getFiles().some(function(file){
                        return /jpe?g$/i.test(file.fsName);
                    });
                }
            }
        }

        if (!folders.length) {
            alert('В указанной папке не было найдено непустых подпапок! Скрипт остановлен.', 'Уведомление');
            return;
        }

        function addWaterMarks(){
            folders.forEach(function(folder){
                var targetFolder = new Folder(folder.absoluteURI + '/../../' + folder.name);
                if (!targetFolder.exists) {
                    targetFolder.create();
                }
                folder.getFiles().forEach(function(file){
                    that.addWatermark(file, Folder.decode(folder.name), targetFolder);
                });
            });
        }

        if (this.hasJpegFiles) {
            this.askJpegQuality(addWaterMarks);
        } else {
            addWaterMarks();
        }



                // open(fileList[i])
                // // use the document name for the layer name in the merged document
                // var docName = app.activeDocument.name
                // // flatten the document so we get everything and then copy
                // app.activeDocument.flatten()
                // app.activeDocument.selection.selectAll()
                // app.activeDocument.selection.copy()
                // // don’t save anything we did
                // app.activeDocument.close(SaveOptions.DONOTSAVECHANGES)
                // // make a random selection on the document to paste into
                // // by dividing the document up in 4 quadrants and pasting
                // // into one of them by selecting that area
                // var topLeftH = Math.floor(Math.random() * 2)
                // var topLeftV = Math.floor(Math.random() * 2)
                // var docH = app.activeDocument.width.value / 2
                // var docV = app.activeDocument.height.value / 2
                // var selRegion = Array(Array(topLeftH * docH, topLeftV * docV),
                // Array(topLeftH * docH + docH, topLeftV * docV),
                // Array(topLeftH * docH + docH, topLeftV * docV + docV),
                // Array(topLeftH * docH, topLeftV * docV + docV),
                // Array(topLeftH * docH, topLeftV * docV))
                // app.activeDocument.selection.select(selRegion)
                // app.activeDocument.paste()
                // // change the layer name and opacity
                // app.activeDocument.activeLayer.name = docName
                // app.activeDocument.activeLayer.fillOpacity = 50
        //     }
        // }

    }

    Watermarker.prototype.addWatermark = function(file, text, targetFolder){
        var doc = app.open(file);
        app.activeDocument = doc;
        this.doc = doc;
        text = text.replace(/^\s*|\s*$/, '');
        if (!/^\d+$/.test(text)) {
            text =  text.replace(/^\d+\s*/, '');
        }
        this.addText(text);//throw new Error('zz');
        this.saveFile(file, new File(targetFolder.absoluteURI + '/' + file.name));
        doc.close(SaveOptions.DONOTSAVECHANGES);
    };
    Watermarker.prototype.saveFile = function(sourceFile, targetFile){
        var extensionLowerCase,
            saveOptions;
        if (/(png)$/i.test(sourceFile.fsName)) {
            saveOptions = new PNGSaveOptions;
            saveOptions.compression = 9;
        } else if (/(jpeg|jpg)$/i.test(sourceFile.fsName)) {
            saveOptions = new JPEGSaveOptions;
            saveOptions.quality = this.jpegQuality;
        } else if (/(gif)$/i.test(sourceFile.fsName)) {
            saveOptions = new GIFSaveOptions;
        } else if (/(bmp)$/i.test(sourceFile.fsName)) {
            saveOptions = new BMPSaveOptions;
        } else if (/(tiff)$/i.test(sourceFile.fsName)) {
            saveOptions = new TiffSaveOptions;
        } else {
            saveOptions = new PhotoshopSaveOptions;
        }
        extensionCase = RegExp.$1 === RegExp.$1.toLowerCase() ? Extension.LOWERCASE : Extension.UPPERCASE;
        if (!(saveOptions instanceof PhotoshopSaveOptions)) {
            this.doc.flatten();
        }
        //alert(targetFolder.absoluteURI + '/' + file.name);
        this.doc.saveAs(targetFile, saveOptions, true, extensionCase);
    };
    Watermarker.prototype.askJpegQuality = function(callback){
        var that = this,
            dlgRes =
                "dialog { properties:{ resizeable:true }, \
                    text: 'Выберите уровень качества', frameLocation:[100,100], \
                    desc: StaticText { \
                        text: 'В исходниках есть JPEG файлы. Выберите для них уровень качества (больше - качественнее, но больше размер)', \
                        properties:{multiline:true} \
                    }, \
                    pnl: Panel { orientation:'row', \
                        sl: Slider { minvalue:0, maxvalue:12}, \
                        st: StaticText { text: ''} \
                    }, \
                    okBtn: Button { text:'ok'} \
                }";
        var w = new Window(dlgRes);

        w.desc.preferredSize = [200, -1];

        w.pnl.st.minimumSize = [20, -1];
        w.pnl.st.text = 12;

        w.pnl.sl.value = 12;
        w.pnl.sl.onChanging = function(){
            this.parent.st.text = Math.floor(this.value);
        };

        w.okBtn.addEventListener('click', function(){
            that.jpegQuality = w.pnl.sl.value;
            callback();
        });

        w.layout.layout(true);
        w.layout.resize();
        w.show();
    };
    Watermarker.prototype.searchFontSize = function(textLayer, desiredWidth, tolerance, diagonal){
        var estimate = 120,
            fontSizeCap = 250,
            doc = this.doc;
        return dih(estimate);

        function check(est){
            var diff,
                width,
                height,
                maxHeight;
            textLayer.textItem.size = est;
            width = textLayer.bounds[2] - textLayer.bounds[0];
            height = textLayer.bounds[3] - textLayer.bounds[1];
            diff = desiredWidth - width;
            if ((diff <= tolerance && diff >= 0) || est >= fontSizeCap) {
                //alert(width + ' ' +  height);
                //maxHeight = (diagonal - width) / 2 * Math.sin(Math.PI / 2) / Math.sin( 2 * Math.PI / 180 * (180 - 90 - 27.3)) +
                //    (diagonal - width) / 2 * Math.sin( 2 * Math.PI / 180 * (90 - 27.3)) / Math.sin( 2 * Math.PI / 180 * 27.3);
                //    maxHeight = new UnitValue(maxHeight, 'in');
                //alert(maxHeight + ' ' + height);
                //if (maxHeight < height) {
                //
                //    desiredWidth = new UnitValue(desiredWidth.value * 0.9, 'in');
                //    return check(est);
                //}
                return 0;
            } else {
                return diff;
            }
        }

        function dih(est, prev){
            var checkVal,
                next;
            if (0 === (checkVal = check(est))) {
                return est;
            }
            if (prev == null && checkVal > 0) {
                return dih(Math.floor(est * 1.5));
            }
            prev == null && (prev = 0);
            if (checkVal < 0) {
                return dih(est - Math.abs(est - prev) / 2, est);
            } else {
                return dih(est + Math.abs(est - prev) / 2, est);
            }
        }
    };
    Watermarker.prototype.addText = function(text){
        var layer = this.doc.artLayers.add(),
            color,
            width,
            height;
        layer.kind = LayerKind.TEXT;
        layer.textItem.contents = text;
        color = new SolidColor;
        color.rgb.red = 255;
        color.rgb.blue = 255;
        color.rgb.green = 255;
        //layer.textItem.color.hexValue = 'ffffff';
        layer.textItem.color = color;
        layer.textItem.size = 72;
        // layer.textItem.color.rgb.red = 255;
        // layer.textItem.color.rgb.green = 255;
        // layer.textItem.color.rgb.blue = 255;
        layer.blendMode = BlendMode.MULTIPLY;
        this.addBlending();
        this.addDropshadow();
        this.setFormatting(text.length, text);

        // layer.textItem.position = new Array(0, 0);
        //alert([this.doc.width, width, this.doc.height, height].join(' '));

        ////
        var that = this;
        var dia = new UnitValue(Math.sqrt(Math.pow(this.doc.width, 2) + Math.pow(this.doc.height, 2)), 'in');
        var desiredWidth = new UnitValue(dia * 0.7, 'in');
        var fontSize = this.searchFontSize(layer, desiredWidth, 8/72, dia);


        layer.textItem.position = [
            (this.doc.width - (layer.bounds[2] - layer.bounds[0])) / 2,
            (this.doc.height + (layer.bounds[3] - layer.bounds[1])) / 2
        ];
        this.rotate(-27.300000);
    };
    Watermarker.prototype.addBlending = function(){
        var idsetd = charIDToTypeID("setd");
        var desc211 = new ActionDescriptor;
        var idnull = charIDToTypeID("null");
            var ref37 = new ActionReference;
            var idLyr = charIDToTypeID("Lyr ");
            var idOrdn = charIDToTypeID("Ordn");
            var idTrgt = charIDToTypeID("Trgt");
            ref37.putEnumerated(idLyr, idOrdn, idTrgt );
        desc211.putReference( idnull, ref37 );
        var idT = charIDToTypeID("T   ");
            var  desc212 = new ActionDescriptor;
            var idMd = charIDToTypeID("Md  ");
            var idBlnM = charIDToTypeID("BlnM");
            var idMltp = charIDToTypeID("Mltp");
            desc212.putEnumerated( idMd, idBlnM, idMltp );
            var idfillOpacity = stringIDToTypeID( "fillOpacity" );
            var idPrc = charIDToTypeID("#Prc");
            desc212.putUnitDouble( idfillOpacity, idPrc, 0.000000 );
            var idLefx = charIDToTypeID("Lefx");
                var desc213 = new ActionDescriptor;
                var idScl = charIDToTypeID("Scl ");
                var idPrc = charIDToTypeID("#Prc");
                desc213.putUnitDouble( idScl, idPrc, 100.000000 );
            var idLefx = charIDToTypeID("Lefx");
            desc212.putObject( idLefx, idLefx, desc213 );
        var idLyr = charIDToTypeID("Lyr ");
        desc211.putObject( idT, idLyr, desc212 );
        executeAction( idsetd, desc211);
    };

    Watermarker.prototype.addDropshadow = function(){
        var idsetd = charIDToTypeID( "setd" );
            var desc214 = new ActionDescriptor;
            var idnull = charIDToTypeID( "null" );
                var ref38 = new ActionReference;
                var idPrpr = charIDToTypeID( "Prpr" );
                var idLefx = charIDToTypeID( "Lefx" );
                ref38.putProperty( idPrpr, idLefx );
                var idLyr = charIDToTypeID( "Lyr " );
                var idOrdn = charIDToTypeID( "Ordn" );
                var idTrgt = charIDToTypeID( "Trgt" );
                ref38.putEnumerated( idLyr, idOrdn, idTrgt );
            desc214.putReference( idnull, ref38 );
            var idT = charIDToTypeID( "T   " );
                var desc215 = new ActionDescriptor;
                var idScl = charIDToTypeID( "Scl " );
                var idPrc = charIDToTypeID( "#Prc" );
                desc215.putUnitDouble( idScl, idPrc, 100.000000 );
                var idDrSh = charIDToTypeID( "DrSh" );
                    var desc216 = new ActionDescriptor;
                    var idenab = charIDToTypeID( "enab" );
                    desc216.putBoolean( idenab, true );
                    var idMd = charIDToTypeID( "Md  " );
                    var idBlnM = charIDToTypeID( "BlnM" );
                    var idMltp = charIDToTypeID( "Mltp" );
                    desc216.putEnumerated( idMd, idBlnM, idMltp );
                    var idClr = charIDToTypeID( "Clr " );
                        var desc217 = new ActionDescriptor;
                        var idRd = charIDToTypeID( "Rd  " );
                        desc217.putDouble( idRd, 0.000000 );
                        var idGrn = charIDToTypeID( "Grn " );
                        desc217.putDouble( idGrn, 0.000000 );
                        var idBl = charIDToTypeID( "Bl  " );
                        desc217.putDouble( idBl, 0.000000 );
                    var idRGBC = charIDToTypeID( "RGBC" );
                    desc216.putObject( idClr, idRGBC, desc217 );
                    var idOpct = charIDToTypeID( "Opct" );
                    var idPrc = charIDToTypeID( "#Prc" );
                    desc216.putUnitDouble( idOpct, idPrc, 75.000000 );
                    var iduglg = charIDToTypeID( "uglg" );
                    desc216.putBoolean( iduglg, true );
                    var idlagl = charIDToTypeID( "lagl" );
                    var idAng = charIDToTypeID( "#Ang" );
                    desc216.putUnitDouble( idlagl, idAng, 120.000000 );
                    var idDstn = charIDToTypeID( "Dstn" );
                    var idPxl = charIDToTypeID( "#Pxl" );
                    desc216.putUnitDouble( idDstn, idPxl, 2.000000 ); //1
                    var idCkmt = charIDToTypeID( "Ckmt" );
                    var idPxl = charIDToTypeID( "#Pxl" );
                    desc216.putUnitDouble( idCkmt, idPxl, 20.000000 ); //0
                    var idblur = charIDToTypeID( "blur" );
                    var idPxl = charIDToTypeID( "#Pxl" );
                    desc216.putUnitDouble( idblur, idPxl, 8.000000 ); //2
                    var idNose = charIDToTypeID( "Nose" );
                    var idPrc = charIDToTypeID( "#Prc" );
                    desc216.putUnitDouble( idNose, idPrc, 0.000000 );
                    var idAntA = charIDToTypeID( "AntA" );
                    desc216.putBoolean( idAntA, false );
                    var idTrnS = charIDToTypeID( "TrnS" );
                        var desc218 = new ActionDescriptor;
                        var idNm = charIDToTypeID( "Nm  " );
                        desc218.putString( idNm, "Linear" );
                    var idShpC = charIDToTypeID( "ShpC" );
                    desc216.putObject( idTrnS, idShpC, desc218 );
                    var idlayerConceals = stringIDToTypeID( "layerConceals" );
                    desc216.putBoolean( idlayerConceals, true );
                var idDrSh = charIDToTypeID( "DrSh" );
                desc215.putObject( idDrSh, idDrSh, desc216 );
            var idLefx = charIDToTypeID( "Lefx" );
            desc214.putObject( idT, idLefx, desc215 );
        executeAction( idsetd, desc214 );
    };
    Watermarker.prototype.setFormatting = function(textLength, text){
        var idsetd = charIDToTypeID( "setd" );
            var desc103 = new ActionDescriptor;
            var idnull = charIDToTypeID( "null" );
                var ref37 = new ActionReference;
                var idTxLr = charIDToTypeID( "TxLr" );
                var idOrdn = charIDToTypeID( "Ordn" );
                var idTrgt = charIDToTypeID( "Trgt" );
                ref37.putEnumerated( idTxLr, idOrdn, idTrgt );
            desc103.putReference( idnull, ref37 );
            var idT = charIDToTypeID( "T   " );
                var desc104 = new ActionDescriptor;
                var idTxt = charIDToTypeID( "Txt " );
                desc104.putString( idTxt, text );
                var idwarp = stringIDToTypeID( "warp" );
                    var desc105 = new ActionDescriptor;
                    var idwarpStyle = stringIDToTypeID( "warpStyle" );
                    var idwarpStyle = stringIDToTypeID( "warpStyle" );
                    var idwarpNone = stringIDToTypeID( "warpNone" );
                    desc105.putEnumerated( idwarpStyle, idwarpStyle, idwarpNone );
                    var idwarpValue = stringIDToTypeID( "warpValue" );
                    desc105.putDouble( idwarpValue, 0.000000 );
                    var idwarpPerspective = stringIDToTypeID( "warpPerspective" );
                    desc105.putDouble( idwarpPerspective, 0.000000 );
                    var idwarpPerspectiveOther = stringIDToTypeID( "warpPerspectiveOther" );
                    desc105.putDouble( idwarpPerspectiveOther, 0.000000 );
                    var idwarpRotate = stringIDToTypeID( "warpRotate" );
                    var idOrnt = charIDToTypeID( "Ornt" );
                    var idHrzn = charIDToTypeID( "Hrzn" );
                    desc105.putEnumerated( idwarpRotate, idOrnt, idHrzn );
                var idwarp = stringIDToTypeID( "warp" );
                desc104.putObject( idwarp, idwarp, desc105 );
                var idtextGridding = stringIDToTypeID( "textGridding" );
                var idtextGridding = stringIDToTypeID( "textGridding" );
                var idNone = charIDToTypeID( "None" );
                desc104.putEnumerated( idtextGridding, idtextGridding, idNone );
                var idOrnt = charIDToTypeID( "Ornt" );
                var idOrnt = charIDToTypeID( "Ornt" );
                var idHrzn = charIDToTypeID( "Hrzn" );
                desc104.putEnumerated( idOrnt, idOrnt, idHrzn );
                var idAntA = charIDToTypeID( "AntA" );
                var idAnnt = charIDToTypeID( "Annt" );
                var idAnCr = charIDToTypeID( "AnCr" );
                desc104.putEnumerated( idAntA, idAnnt, idAnCr );
                var idbounds = stringIDToTypeID( "bounds" );
                    var desc106 = new ActionDescriptor;
                    var idLeft = charIDToTypeID( "Left" );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc106.putUnitDouble( idLeft, idPnt, 0.000000 );
                    var idTop = charIDToTypeID( "Top " );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc106.putUnitDouble( idTop, idPnt, -59.835938 );
                    var idRght = charIDToTypeID( "Rght" );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc106.putUnitDouble( idRght, idPnt, 748.714233 );
                    var idBtom = charIDToTypeID( "Btom" );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc106.putUnitDouble( idBtom, idPnt, 22.078125 );
                var idbounds = stringIDToTypeID( "bounds" );
                desc104.putObject( idbounds, idbounds, desc106 );
                var idboundingBox = stringIDToTypeID( "boundingBox" );
                    var desc107 = new ActionDescriptor;
                    var idLeft = charIDToTypeID( "Left" );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc107.putUnitDouble( idLeft, idPnt, 1.000000 );
                    var idTop = charIDToTypeID( "Top " );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc107.putUnitDouble( idTop, idPnt, -49.000000 );
                    var idRght = charIDToTypeID( "Rght" );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc107.putUnitDouble( idRght, idPnt, 730.438110 );
                    var idBtom = charIDToTypeID( "Btom" );
                    var idPnt = charIDToTypeID( "#Pnt" );
                    desc107.putUnitDouble( idBtom, idPnt, 1.000000 );
                var idboundingBox = stringIDToTypeID( "boundingBox" );
                desc104.putObject( idboundingBox, idboundingBox, desc107 );
                var idtextShape = stringIDToTypeID( "textShape" );
                    var list29 = new ActionList;
                        var desc108 = new ActionDescriptor;
                        var idTEXT = charIDToTypeID( "TEXT" );
                        var idTEXT = charIDToTypeID( "TEXT" );
                        var idPnt = charIDToTypeID( "Pnt " );
                        desc108.putEnumerated( idTEXT, idTEXT, idPnt );
                        var idOrnt = charIDToTypeID( "Ornt" );
                        var idOrnt = charIDToTypeID( "Ornt" );
                        var idHrzn = charIDToTypeID( "Hrzn" );
                        desc108.putEnumerated( idOrnt, idOrnt, idHrzn );
                        var idTrnf = charIDToTypeID( "Trnf" );
                            var desc109 = new ActionDescriptor;
                            var idxx = stringIDToTypeID( "xx" );
                            desc109.putDouble( idxx, 1.000000 );
                            var idxy = stringIDToTypeID( "xy" );
                            desc109.putDouble( idxy, 0.000000 );
                            var idyx = stringIDToTypeID( "yx" );
                            desc109.putDouble( idyx, 0.000000 );
                            var idyy = stringIDToTypeID( "yy" );
                            desc109.putDouble( idyy, 1.000000 );
                            var idtx = stringIDToTypeID( "tx" );
                            desc109.putDouble( idtx, 0.000000 );
                            var idty = stringIDToTypeID( "ty" );
                            desc109.putDouble( idty, 0.000000 );
                        var idTrnf = charIDToTypeID( "Trnf" );
                        desc108.putObject( idTrnf, idTrnf, desc109 );
                        var idrowCount = stringIDToTypeID( "rowCount" );
                        desc108.putInteger( idrowCount, 1 );
                        var idcolumnCount = stringIDToTypeID( "columnCount" );
                        desc108.putInteger( idcolumnCount, 1 );
                        var idrowMajorOrder = stringIDToTypeID( "rowMajorOrder" );
                        desc108.putBoolean( idrowMajorOrder, true );
                        var idrowGutter = stringIDToTypeID( "rowGutter" );
                        var idPnt = charIDToTypeID( "#Pnt" );
                        desc108.putUnitDouble( idrowGutter, idPnt, 0.000000 );
                        var idcolumnGutter = stringIDToTypeID( "columnGutter" );
                        var idPnt = charIDToTypeID( "#Pnt" );
                        desc108.putUnitDouble( idcolumnGutter, idPnt, 0.000000 );
                        var idSpcn = charIDToTypeID( "Spcn" );
                        var idPnt = charIDToTypeID( "#Pnt" );
                        desc108.putUnitDouble( idSpcn, idPnt, 0.000000 );
                        var idframeBaselineAlignment = stringIDToTypeID( "frameBaselineAlignment" );
                        var idframeBaselineAlignment = stringIDToTypeID( "frameBaselineAlignment" );
                        var idalignByAscent = stringIDToTypeID( "alignByAscent" );
                        desc108.putEnumerated( idframeBaselineAlignment, idframeBaselineAlignment, idalignByAscent );
                        var idfirstBaselineMinimum = stringIDToTypeID( "firstBaselineMinimum" );
                        var idPnt = charIDToTypeID( "#Pnt" );
                        desc108.putUnitDouble( idfirstBaselineMinimum, idPnt, 0.000000 );
                        var idbase = stringIDToTypeID( "base" );
                            var desc110 = new ActionDescriptor;
                            var idHrzn = charIDToTypeID( "Hrzn" );
                            desc110.putDouble( idHrzn, 0.000000 );
                            var idVrtc = charIDToTypeID( "Vrtc" );
                            desc110.putDouble( idVrtc, 0.000000 );
                        var idPnt = charIDToTypeID( "Pnt " );
                        desc108.putObject( idbase, idPnt, desc110 );
                    var idtextShape = stringIDToTypeID( "textShape" );
                    list29.putObject( idtextShape, desc108 );
                desc104.putList( idtextShape, list29 );
                var idTxtt = charIDToTypeID( "Txtt" );
                    var list30 = new ActionList;
                        var desc111 = new ActionDescriptor;
                        var idFrom = charIDToTypeID( "From" );
                        desc111.putInteger( idFrom, 0 );
                        var idT = charIDToTypeID( "T   " );
                        desc111.putInteger( idT, textLength );
                        var idTxtS = charIDToTypeID( "TxtS" );
                            var desc112 = new ActionDescriptor;
                            var idstyleSheetHasParent = stringIDToTypeID( "styleSheetHasParent" );
                            desc112.putBoolean( idstyleSheetHasParent, true );
                            var idfontPostScriptName = stringIDToTypeID( "fontPostScriptName" );
                            desc112.putString( idfontPostScriptName, "ArialMT" );
                            var idFntN = charIDToTypeID( "FntN" );
                            desc112.putString( idFntN, "Arial" );
                            var idFntS = charIDToTypeID( "FntS" );
                            desc112.putString( idFntS, "Regular" );
                            var idScrp = charIDToTypeID( "Scrp" );
                            desc112.putInteger( idScrp, 0 );
                            var idFntT = charIDToTypeID( "FntT" );
                            desc112.putInteger( idFntT, 1 );
                            var idSz = charIDToTypeID( "Sz  " );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc112.putUnitDouble( idSz, idPnt, 72.000000 );
                            var idHrzS = charIDToTypeID( "HrzS" );
                            desc112.putDouble( idHrzS, 100.000000 );
                            var idVrtS = charIDToTypeID( "VrtS" );
                            desc112.putDouble( idVrtS, 100.000000 );
                            var idsyntheticBold = stringIDToTypeID( "syntheticBold" );
                            desc112.putBoolean( idsyntheticBold, false );
                            var idsyntheticItalic = stringIDToTypeID( "syntheticItalic" );
                            desc112.putBoolean( idsyntheticItalic, false );
                            var idautoLeading = stringIDToTypeID( "autoLeading" );
                            desc112.putBoolean( idautoLeading, true );
                            var idTrck = charIDToTypeID( "Trck" );
                            desc112.putInteger( idTrck, 240 );
                            var idBsln = charIDToTypeID( "Bsln" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc112.putUnitDouble( idBsln, idPnt, 0.000000 );
                            var idAtKr = charIDToTypeID( "AtKr" );
                            var idAtKr = charIDToTypeID( "AtKr" );
                            var idmetricsKern = stringIDToTypeID( "metricsKern" );
                            desc112.putEnumerated( idAtKr, idAtKr, idmetricsKern );
                            var idfontCaps = stringIDToTypeID( "fontCaps" );
                            var idfontCaps = stringIDToTypeID( "fontCaps" );
                            var idNrml = charIDToTypeID( "Nrml" );
                            desc112.putEnumerated( idfontCaps, idfontCaps, idNrml );
                            var iddigitSet = stringIDToTypeID( "digitSet" );
                            var iddigitSet = stringIDToTypeID( "digitSet" );
                            var iddefaultDigits = stringIDToTypeID( "defaultDigits" );
                            desc112.putEnumerated( iddigitSet, iddigitSet, iddefaultDigits );
                            var idkashidas = stringIDToTypeID( "kashidas" );
                            var idkashidas = stringIDToTypeID( "kashidas" );
                            var idkashidaDefault = stringIDToTypeID( "kashidaDefault" );
                            desc112.putEnumerated( idkashidas, idkashidas, idkashidaDefault );
                            var iddiacXOffset = stringIDToTypeID( "diacXOffset" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc112.putUnitDouble( iddiacXOffset, idPnt, 0.000000 );
                            var iddiacYOffset = stringIDToTypeID( "diacYOffset" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc112.putUnitDouble( iddiacYOffset, idPnt, 0.000000 );
                            var idmarkYDistFromBaseline = stringIDToTypeID( "markYDistFromBaseline" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc112.putUnitDouble( idmarkYDistFromBaseline, idPnt, 100.000000 );
                            var idbaseline = stringIDToTypeID( "baseline" );
                            var idbaseline = stringIDToTypeID( "baseline" );
                            var idNrml = charIDToTypeID( "Nrml" );
                            desc112.putEnumerated( idbaseline, idbaseline, idNrml );
                            var idstrikethrough = stringIDToTypeID( "strikethrough" );
                            var idstrikethrough = stringIDToTypeID( "strikethrough" );
                            var idstrikethroughOff = stringIDToTypeID( "strikethroughOff" );
                            desc112.putEnumerated( idstrikethrough, idstrikethrough, idstrikethroughOff );
                            var idUndl = charIDToTypeID( "Undl" );
                            var idUndl = charIDToTypeID( "Undl" );
                            var idunderlineOff = stringIDToTypeID( "underlineOff" );
                            desc112.putEnumerated( idUndl, idUndl, idunderlineOff );
                            var idligature = stringIDToTypeID( "ligature" );
                            desc112.putBoolean( idligature, true );
                            var idaltligature = stringIDToTypeID( "altligature" );
                            desc112.putBoolean( idaltligature, false );
                            var idcontextualLigatures = stringIDToTypeID( "contextualLigatures" );
                            desc112.putBoolean( idcontextualLigatures, false );
                            var idfractions = stringIDToTypeID( "fractions" );
                            desc112.putBoolean( idfractions, false );
                            var idordinals = stringIDToTypeID( "ordinals" );
                            desc112.putBoolean( idordinals, false );
                            var idswash = stringIDToTypeID( "swash" );
                            desc112.putBoolean( idswash, false );
                            var idtitling = stringIDToTypeID( "titling" );
                            desc112.putBoolean( idtitling, false );
                            var idconnectionForms = stringIDToTypeID( "connectionForms" );
                            desc112.putBoolean( idconnectionForms, false );
                            var idstylisticAlternates = stringIDToTypeID( "stylisticAlternates" );
                            desc112.putBoolean( idstylisticAlternates, false );
                            var idornaments = stringIDToTypeID( "ornaments" );
                            desc112.putBoolean( idornaments, false );
                            var idjustificationAlternates = stringIDToTypeID( "justificationAlternates" );
                            desc112.putBoolean( idjustificationAlternates, false );
                            var idfigureStyle = stringIDToTypeID( "figureStyle" );
                            var idfigureStyle = stringIDToTypeID( "figureStyle" );
                            var idNrml = charIDToTypeID( "Nrml" );
                            desc112.putEnumerated( idfigureStyle, idfigureStyle, idNrml );
                            var idproportionalMetrics = stringIDToTypeID( "proportionalMetrics" );
                            desc112.putBoolean( idproportionalMetrics, false );
                            var idkana = stringIDToTypeID( "kana" );
                            desc112.putBoolean( idkana, false );
                            var iditalics = stringIDToTypeID( "italics" );
                            desc112.putBoolean( iditalics, false );
                            var idbaselineDirection = stringIDToTypeID( "baselineDirection" );
                            var idbaselineDirection = stringIDToTypeID( "baselineDirection" );
                            var idwithStream = stringIDToTypeID( "withStream" );
                            desc112.putEnumerated( idbaselineDirection, idbaselineDirection, idwithStream );
                            var idtextLanguage = stringIDToTypeID( "textLanguage" );
                            var idtextLanguage = stringIDToTypeID( "textLanguage" );
                            var idenglishLanguage = stringIDToTypeID( "englishLanguage" );
                            desc112.putEnumerated( idtextLanguage, idtextLanguage, idenglishLanguage );
                            var idjapaneseAlternate = stringIDToTypeID( "japaneseAlternate" );
                            var idjapaneseAlternate = stringIDToTypeID( "japaneseAlternate" );
                            var iddefaultForm = stringIDToTypeID( "defaultForm" );
                            desc112.putEnumerated( idjapaneseAlternate, idjapaneseAlternate, iddefaultForm );
                            var idmojiZume = stringIDToTypeID( "mojiZume" );
                            desc112.putDouble( idmojiZume, 0.000000 );
                            var idgridAlignment = stringIDToTypeID( "gridAlignment" );
                            var idgridAlignment = stringIDToTypeID( "gridAlignment" );
                            var idroman = stringIDToTypeID( "roman" );
                            desc112.putEnumerated( idgridAlignment, idgridAlignment, idroman );
                            var idnoBreak = stringIDToTypeID( "noBreak" );
                            desc112.putBoolean( idnoBreak, false );
                            var idClr = charIDToTypeID( "Clr " );
                                var desc113 = new ActionDescriptor;
                                var idRd = charIDToTypeID( "Rd  " );
                                desc113.putDouble( idRd, 255.000000 );
                                var idGrn = charIDToTypeID( "Grn " );
                                desc113.putDouble( idGrn, 255.000000 );
                                var idBl = charIDToTypeID( "Bl  " );
                                desc113.putDouble( idBl, 255.000000 );
                            var idRGBC = charIDToTypeID( "RGBC" );
                            desc112.putObject( idClr, idRGBC, desc113 );
                            var idstrokeColor = stringIDToTypeID( "strokeColor" );
                                var desc114 = new ActionDescriptor;
                                var idRd = charIDToTypeID( "Rd  " );
                                desc114.putDouble( idRd, 0.000000 );
                                var idGrn = charIDToTypeID( "Grn " );
                                desc114.putDouble( idGrn, 0.000000 );
                                var idBl = charIDToTypeID( "Bl  " );
                                desc114.putDouble( idBl, 0.000000 );
                            var idRGBC = charIDToTypeID( "RGBC" );
                            desc112.putObject( idstrokeColor, idRGBC, desc114 );
                            var idbaseParentStyle = stringIDToTypeID( "baseParentStyle" );
                                var desc115 = new ActionDescriptor;
                                var idfontPostScriptName = stringIDToTypeID( "fontPostScriptName" );
                                desc115.putString( idfontPostScriptName, "MyriadPro-Regular" );
                                var idFntN = charIDToTypeID( "FntN" );
                                desc115.putString( idFntN, "Myriad Pro" );
                                var idFntS = charIDToTypeID( "FntS" );
                                desc115.putString( idFntS, "Regular" );
                                var idScrp = charIDToTypeID( "Scrp" );
                                desc115.putInteger( idScrp, 0 );
                                var idFntT = charIDToTypeID( "FntT" );
                                desc115.putInteger( idFntT, 0 );
                                var idSz = charIDToTypeID( "Sz  " );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( idSz, idPnt, 12.000000 );
                                var idHrzS = charIDToTypeID( "HrzS" );
                                desc115.putDouble( idHrzS, 100.000000 );
                                var idVrtS = charIDToTypeID( "VrtS" );
                                desc115.putDouble( idVrtS, 100.000000 );
                                var idsyntheticBold = stringIDToTypeID( "syntheticBold" );
                                desc115.putBoolean( idsyntheticBold, false );
                                var idsyntheticItalic = stringIDToTypeID( "syntheticItalic" );
                                desc115.putBoolean( idsyntheticItalic, false );
                                var idautoLeading = stringIDToTypeID( "autoLeading" );
                                desc115.putBoolean( idautoLeading, true );
                                var idTrck = charIDToTypeID( "Trck" );
                                desc115.putInteger( idTrck, 0 );
                                var idBsln = charIDToTypeID( "Bsln" );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( idBsln, idPnt, 0.000000 );
                                var idcharacterRotation = stringIDToTypeID( "characterRotation" );
                                desc115.putDouble( idcharacterRotation, 0.000000 );
                                var idAtKr = charIDToTypeID( "AtKr" );
                                var idAtKr = charIDToTypeID( "AtKr" );
                                var idmetricsKern = stringIDToTypeID( "metricsKern" );
                                desc115.putEnumerated( idAtKr, idAtKr, idmetricsKern );
                                var idfontCaps = stringIDToTypeID( "fontCaps" );
                                var idfontCaps = stringIDToTypeID( "fontCaps" );
                                var idNrml = charIDToTypeID( "Nrml" );
                                desc115.putEnumerated( idfontCaps, idfontCaps, idNrml );
                                var iddigitSet = stringIDToTypeID( "digitSet" );
                                var iddigitSet = stringIDToTypeID( "digitSet" );
                                var iddefaultDigits = stringIDToTypeID( "defaultDigits" );
                                desc115.putEnumerated( iddigitSet, iddigitSet, iddefaultDigits );
                                var iddirOverride = stringIDToTypeID( "dirOverride" );
                                var iddirOverride = stringIDToTypeID( "dirOverride" );
                                var iddirOverrideDefault = stringIDToTypeID( "dirOverrideDefault" );
                                desc115.putEnumerated( iddirOverride, iddirOverride, iddirOverrideDefault );
                                var idkashidas = stringIDToTypeID( "kashidas" );
                                var idkashidas = stringIDToTypeID( "kashidas" );
                                var idkashidaDefault = stringIDToTypeID( "kashidaDefault" );
                                desc115.putEnumerated( idkashidas, idkashidas, idkashidaDefault );
                                var iddiacVPos = stringIDToTypeID( "diacVPos" );
                                var iddiacVPos = stringIDToTypeID( "diacVPos" );
                                var iddiacVPosOpenType = stringIDToTypeID( "diacVPosOpenType" );
                                desc115.putEnumerated( iddiacVPos, iddiacVPos, iddiacVPosOpenType );
                                var iddiacXOffset = stringIDToTypeID( "diacXOffset" );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( iddiacXOffset, idPnt, 0.000000 );
                                var iddiacYOffset = stringIDToTypeID( "diacYOffset" );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( iddiacYOffset, idPnt, 0.000000 );
                                var idmarkYDistFromBaseline = stringIDToTypeID( "markYDistFromBaseline" );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( idmarkYDistFromBaseline, idPnt, 100.000000 );
                                var idbaseline = stringIDToTypeID( "baseline" );
                                var idbaseline = stringIDToTypeID( "baseline" );
                                var idNrml = charIDToTypeID( "Nrml" );
                                desc115.putEnumerated( idbaseline, idbaseline, idNrml );
                                var idotbaseline = stringIDToTypeID( "otbaseline" );
                                var idotbaseline = stringIDToTypeID( "otbaseline" );
                                var idNrml = charIDToTypeID( "Nrml" );
                                desc115.putEnumerated( idotbaseline, idotbaseline, idNrml );
                                var idstrikethrough = stringIDToTypeID( "strikethrough" );
                                var idstrikethrough = stringIDToTypeID( "strikethrough" );
                                var idstrikethroughOff = stringIDToTypeID( "strikethroughOff" );
                                desc115.putEnumerated( idstrikethrough, idstrikethrough, idstrikethroughOff );
                                var idUndl = charIDToTypeID( "Undl" );
                                var idUndl = charIDToTypeID( "Undl" );
                                var idunderlineOff = stringIDToTypeID( "underlineOff" );
                                desc115.putEnumerated( idUndl, idUndl, idunderlineOff );
                                var idunderlineOffset = stringIDToTypeID( "underlineOffset" );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( idunderlineOffset, idPnt, 0.000000 );
                                var idligature = stringIDToTypeID( "ligature" );
                                desc115.putBoolean( idligature, true );
                                var idaltligature = stringIDToTypeID( "altligature" );
                                desc115.putBoolean( idaltligature, false );
                                var idcontextualLigatures = stringIDToTypeID( "contextualLigatures" );
                                desc115.putBoolean( idcontextualLigatures, false );
                                var idalternateLigatures = stringIDToTypeID( "alternateLigatures" );
                                desc115.putBoolean( idalternateLigatures, false );
                                var idoldStyle = stringIDToTypeID( "oldStyle" );
                                desc115.putBoolean( idoldStyle, false );
                                var idfractions = stringIDToTypeID( "fractions" );
                                desc115.putBoolean( idfractions, false );
                                var idordinals = stringIDToTypeID( "ordinals" );
                                desc115.putBoolean( idordinals, false );
                                var idswash = stringIDToTypeID( "swash" );
                                desc115.putBoolean( idswash, false );
                                var idtitling = stringIDToTypeID( "titling" );
                                desc115.putBoolean( idtitling, false );
                                var idconnectionForms = stringIDToTypeID( "connectionForms" );
                                desc115.putBoolean( idconnectionForms, false );
                                var idstylisticAlternates = stringIDToTypeID( "stylisticAlternates" );
                                desc115.putBoolean( idstylisticAlternates, false );
                                var idornaments = stringIDToTypeID( "ornaments" );
                                desc115.putBoolean( idornaments, false );
                                var idjustificationAlternates = stringIDToTypeID( "justificationAlternates" );
                                desc115.putBoolean( idjustificationAlternates, false );
                                var idfigureStyle = stringIDToTypeID( "figureStyle" );
                                var idfigureStyle = stringIDToTypeID( "figureStyle" );
                                var idNrml = charIDToTypeID( "Nrml" );
                                desc115.putEnumerated( idfigureStyle, idfigureStyle, idNrml );
                                var idproportionalMetrics = stringIDToTypeID( "proportionalMetrics" );
                                desc115.putBoolean( idproportionalMetrics, false );
                                var idkana = stringIDToTypeID( "kana" );
                                desc115.putBoolean( idkana, false );
                                var iditalics = stringIDToTypeID( "italics" );
                                desc115.putBoolean( iditalics, false );
                                var idruby = stringIDToTypeID( "ruby" );
                                desc115.putBoolean( idruby, false );
                                var idbaselineDirection = stringIDToTypeID( "baselineDirection" );
                                var idbaselineDirection = stringIDToTypeID( "baselineDirection" );
                                var idrotated = stringIDToTypeID( "rotated" );
                                desc115.putEnumerated( idbaselineDirection, idbaselineDirection, idrotated );
                                var idtextLanguage = stringIDToTypeID( "textLanguage" );
                                var idtextLanguage = stringIDToTypeID( "textLanguage" );
                                var idenglishLanguage = stringIDToTypeID( "englishLanguage" );
                                desc115.putEnumerated( idtextLanguage, idtextLanguage, idenglishLanguage );
                                var idjapaneseAlternate = stringIDToTypeID( "japaneseAlternate" );
                                var idjapaneseAlternate = stringIDToTypeID( "japaneseAlternate" );
                                var iddefaultForm = stringIDToTypeID( "defaultForm" );
                                desc115.putEnumerated( idjapaneseAlternate, idjapaneseAlternate, iddefaultForm );
                                var idmojiZume = stringIDToTypeID( "mojiZume" );
                                desc115.putDouble( idmojiZume, 0.000000 );
                                var idgridAlignment = stringIDToTypeID( "gridAlignment" );
                                var idgridAlignment = stringIDToTypeID( "gridAlignment" );
                                var idroman = stringIDToTypeID( "roman" );
                                desc115.putEnumerated( idgridAlignment, idgridAlignment, idroman );
                                var idenableWariChu = stringIDToTypeID( "enableWariChu" );
                                desc115.putBoolean( idenableWariChu, false );
                                var idwariChuCount = stringIDToTypeID( "wariChuCount" );
                                desc115.putInteger( idwariChuCount, 2 );
                                var idwariChuLineGap = stringIDToTypeID( "wariChuLineGap" );
                                desc115.putInteger( idwariChuLineGap, 0 );
                                var idwariChuScale = stringIDToTypeID( "wariChuScale" );
                                desc115.putDouble( idwariChuScale, 0.500000 );
                                var idwariChuWidow = stringIDToTypeID( "wariChuWidow" );
                                desc115.putInteger( idwariChuWidow, 2 );
                                var idwariChuOrphan = stringIDToTypeID( "wariChuOrphan" );
                                desc115.putInteger( idwariChuOrphan, 2 );
                                var idwariChuJustification = stringIDToTypeID( "wariChuJustification" );
                                var idwariChuJustification = stringIDToTypeID( "wariChuJustification" );
                                var idwariChuAutoJustify = stringIDToTypeID( "wariChuAutoJustify" );
                                desc115.putEnumerated( idwariChuJustification, idwariChuJustification, idwariChuAutoJustify );
                                var idtcyUpDown = stringIDToTypeID( "tcyUpDown" );
                                desc115.putInteger( idtcyUpDown, 0 );
                                var idtcyLeftRight = stringIDToTypeID( "tcyLeftRight" );
                                desc115.putInteger( idtcyLeftRight, 0 );
                                var idleftAki = stringIDToTypeID( "leftAki" );
                                desc115.putDouble( idleftAki, -1.000000 );
                                var idrightAki = stringIDToTypeID( "rightAki" );
                                desc115.putDouble( idrightAki, -1.000000 );
                                var idjiDori = stringIDToTypeID( "jiDori" );
                                desc115.putInteger( idjiDori, 0 );
                                var idnoBreak = stringIDToTypeID( "noBreak" );
                                desc115.putBoolean( idnoBreak, false );
                                var idClr = charIDToTypeID( "Clr " );
                                    var desc116 = new ActionDescriptor;
                                    var idRd = charIDToTypeID( "Rd  " );
                                    desc116.putDouble( idRd, 0.000000 );
                                    var idGrn = charIDToTypeID( "Grn " );
                                    desc116.putDouble( idGrn, 0.000000 );
                                    var idBl = charIDToTypeID( "Bl  " );
                                    desc116.putDouble( idBl, 0.000000 );
                                var idRGBC = charIDToTypeID( "RGBC" );
                                desc115.putObject( idClr, idRGBC, desc116 );
                                var idstrokeColor = stringIDToTypeID( "strokeColor" );
                                    var desc117 = new ActionDescriptor;
                                    var idRd = charIDToTypeID( "Rd  " );
                                    desc117.putDouble( idRd, 0.000000 );
                                    var idGrn = charIDToTypeID( "Grn " );
                                    desc117.putDouble( idGrn, 0.000000 );
                                    var idBl = charIDToTypeID( "Bl  " );
                                    desc117.putDouble( idBl, 0.000000 );
                                var idRGBC = charIDToTypeID( "RGBC" );
                                desc115.putObject( idstrokeColor, idRGBC, desc117 );
                                var idFl = charIDToTypeID( "Fl  " );
                                desc115.putBoolean( idFl, true );
                                var idStrk = charIDToTypeID( "Strk" );
                                desc115.putBoolean( idStrk, false );
                                var idfillFirst = stringIDToTypeID( "fillFirst" );
                                desc115.putBoolean( idfillFirst, true );
                                var idfillOverPrint = stringIDToTypeID( "fillOverPrint" );
                                desc115.putBoolean( idfillOverPrint, false );
                                var idstrokeOverPrint = stringIDToTypeID( "strokeOverPrint" );
                                desc115.putBoolean( idstrokeOverPrint, false );
                                var idlineCap = stringIDToTypeID( "lineCap" );
                                var idlineCap = stringIDToTypeID( "lineCap" );
                                var idbuttCap = stringIDToTypeID( "buttCap" );
                                desc115.putEnumerated( idlineCap, idlineCap, idbuttCap );
                                var idlineJoin = stringIDToTypeID( "lineJoin" );
                                var idlineJoin = stringIDToTypeID( "lineJoin" );
                                var idmiterJoin = stringIDToTypeID( "miterJoin" );
                                desc115.putEnumerated( idlineJoin, idlineJoin, idmiterJoin );
                                var idlineWidth = stringIDToTypeID( "lineWidth" );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( idlineWidth, idPnt, 1.000000 );
                                var idmiterLimit = stringIDToTypeID( "miterLimit" );
                                var idPnt = charIDToTypeID( "#Pnt" );
                                desc115.putUnitDouble( idmiterLimit, idPnt, 4.000000 );
                                var idlineDashoffset = stringIDToTypeID( "lineDashoffset" );
                                desc115.putDouble( idlineDashoffset, 0.000000 );
                            var idTxtS = charIDToTypeID( "TxtS" );
                            desc112.putObject( idbaseParentStyle, idTxtS, desc115 );
                        var idTxtS = charIDToTypeID( "TxtS" );
                        desc111.putObject( idTxtS, idTxtS, desc112 );
                    var idTxtt = charIDToTypeID( "Txtt" );
                    list30.putObject( idTxtt, desc111 );
                desc104.putList( idTxtt, list30 );
                var idparagraphStyleRange = stringIDToTypeID( "paragraphStyleRange" );
                    var list31 = new ActionList;
                        var desc118 = new ActionDescriptor;
                        var idFrom = charIDToTypeID( "From" );
                        desc118.putInteger( idFrom, 0 );
                        var idT = charIDToTypeID( "T   " );
                        desc118.putInteger( idT, 14 );
                        var idparagraphStyle = stringIDToTypeID( "paragraphStyle" );
                            var desc119 = new ActionDescriptor;
                            var idstyleSheetHasParent = stringIDToTypeID( "styleSheetHasParent" );
                            desc119.putBoolean( idstyleSheetHasParent, true );
                            var idAlgn = charIDToTypeID( "Algn" );
                            var idAlg = charIDToTypeID( "Alg " );
                            var idLeft = charIDToTypeID( "Left" );
                            desc119.putEnumerated( idAlgn, idAlg, idLeft );
                            var idfirstLineIndent = stringIDToTypeID( "firstLineIndent" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc119.putUnitDouble( idfirstLineIndent, idPnt, 0.000000 );
                            var idstartIndent = stringIDToTypeID( "startIndent" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc119.putUnitDouble( idstartIndent, idPnt, 0.000000 );
                            var idendIndent = stringIDToTypeID( "endIndent" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc119.putUnitDouble( idendIndent, idPnt, 0.000000 );
                            var idspaceBefore = stringIDToTypeID( "spaceBefore" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc119.putUnitDouble( idspaceBefore, idPnt, 0.000000 );
                            var idspaceAfter = stringIDToTypeID( "spaceAfter" );
                            var idPnt = charIDToTypeID( "#Pnt" );
                            desc119.putUnitDouble( idspaceAfter, idPnt, 0.000000 );
                            var idautoLeadingPercentage = stringIDToTypeID( "autoLeadingPercentage" );
                            desc119.putDouble( idautoLeadingPercentage, 1.200000 );
                            var idleadingType = stringIDToTypeID( "leadingType" );
                            var idleadingType = stringIDToTypeID( "leadingType" );
                            var idleadingBelow = stringIDToTypeID( "leadingBelow" );
                            desc119.putEnumerated( idleadingType, idleadingType, idleadingBelow );
                            var iddirectionType = stringIDToTypeID( "directionType" );
                            var iddirectionType = stringIDToTypeID( "directionType" );
                            var iddirLeftToRight = stringIDToTypeID( "dirLeftToRight" );
                            desc119.putEnumerated( iddirectionType, iddirectionType, iddirLeftToRight );
                            var idkashidaWidthType = stringIDToTypeID( "kashidaWidthType" );
                            var idkashidaWidthType = stringIDToTypeID( "kashidaWidthType" );
                            var idkashidaWidthMedium = stringIDToTypeID( "kashidaWidthMedium" );
                            desc119.putEnumerated( idkashidaWidthType, idkashidaWidthType, idkashidaWidthMedium );
                            var idhyphenate = stringIDToTypeID( "hyphenate" );
                            desc119.putBoolean( idhyphenate, true );
                            var idhyphenateWordSize = stringIDToTypeID( "hyphenateWordSize" );
                            desc119.putInteger( idhyphenateWordSize, 6 );
                            var idhyphenatePreLength = stringIDToTypeID( "hyphenatePreLength" );
                            desc119.putInteger( idhyphenatePreLength, 2 );
                            var idhyphenatePostLength = stringIDToTypeID( "hyphenatePostLength" );
                            desc119.putInteger( idhyphenatePostLength, 2 );
                            var idhyphenateLimit = stringIDToTypeID( "hyphenateLimit" );
                            desc119.putInteger( idhyphenateLimit, 0 );
                            var idhyphenationZone = stringIDToTypeID( "hyphenationZone" );
                            desc119.putDouble( idhyphenationZone, 36.000000 );
                            var idhyphenateCapitalized = stringIDToTypeID( "hyphenateCapitalized" );
                            desc119.putBoolean( idhyphenateCapitalized, true );
                            var idjustificationWordMinimum = stringIDToTypeID( "justificationWordMinimum" );
                            desc119.putDouble( idjustificationWordMinimum, 0.800000 );
                            var idjustificationWordDesired = stringIDToTypeID( "justificationWordDesired" );
                            desc119.putDouble( idjustificationWordDesired, 1.000000 );
                            var idjustificationWordMaximum = stringIDToTypeID( "justificationWordMaximum" );
                            desc119.putDouble( idjustificationWordMaximum, 1.330000 );
                            var idjustificationLetterMinimum = stringIDToTypeID( "justificationLetterMinimum" );
                            desc119.putDouble( idjustificationLetterMinimum, 0.000000 );
                            var idjustificationLetterDesired = stringIDToTypeID( "justificationLetterDesired" );
                            desc119.putDouble( idjustificationLetterDesired, 0.000000 );
                            var idjustificationLetterMaximum = stringIDToTypeID( "justificationLetterMaximum" );
                            desc119.putDouble( idjustificationLetterMaximum, 0.000000 );
                            var idjustificationGlyphMinimum = stringIDToTypeID( "justificationGlyphMinimum" );
                            desc119.putDouble( idjustificationGlyphMinimum, 1.000000 );
                            var idjustificationGlyphDesired = stringIDToTypeID( "justificationGlyphDesired" );
                            desc119.putDouble( idjustificationGlyphDesired, 1.000000 );
                            var idjustificationGlyphMaximum = stringIDToTypeID( "justificationGlyphMaximum" );
                            desc119.putDouble( idjustificationGlyphMaximum, 1.000000 );
                            var idhangingRoman = stringIDToTypeID( "hangingRoman" );
                            desc119.putBoolean( idhangingRoman, false );
                            var idburasagari = stringIDToTypeID( "burasagari" );
                            var idburasagari = stringIDToTypeID( "burasagari" );
                            var idburasagariStandard = stringIDToTypeID( "burasagariStandard" );
                            desc119.putEnumerated( idburasagari, idburasagari, idburasagariStandard );
                            var idpreferredKinsokuOrder = stringIDToTypeID( "preferredKinsokuOrder" );
                            var idpreferredKinsokuOrder = stringIDToTypeID( "preferredKinsokuOrder" );
                            var idpushIn = stringIDToTypeID( "pushIn" );
                            desc119.putEnumerated( idpreferredKinsokuOrder, idpreferredKinsokuOrder, idpushIn );
                            var idtextEveryLineComposer = stringIDToTypeID( "textEveryLineComposer" );
                            desc119.putBoolean( idtextEveryLineComposer, true );
                            var idtextComposerEngine = stringIDToTypeID( "textComposerEngine" );
                            var idtextComposerEngine = stringIDToTypeID( "textComposerEngine" );
                            var idtextLatinCJKComposer = stringIDToTypeID( "textLatinCJKComposer" );
                            desc119.putEnumerated( idtextComposerEngine, idtextComposerEngine, idtextLatinCJKComposer );
                        var idparagraphStyle = stringIDToTypeID( "paragraphStyle" );
                        desc118.putObject( idparagraphStyle, idparagraphStyle, desc119 );
                    var idparagraphStyleRange = stringIDToTypeID( "paragraphStyleRange" );
                    list31.putObject( idparagraphStyleRange, desc118 );
                desc104.putList( idparagraphStyleRange, list31 );
                var idkerningRange = stringIDToTypeID( "kerningRange" );
                    var list32 = new ActionList;
                desc104.putList( idkerningRange, list32 );
            var idTxLr = charIDToTypeID( "TxLr" );
            desc103.putObject( idT, idTxLr, desc104 );
        executeAction( idsetd, desc103 );
    };

    Watermarker.prototype.rotate = function(angle){
        var idTrnf = charIDToTypeID( "Trnf" );
            var desc93 = new ActionDescriptor();
            var idnull = charIDToTypeID( "null" );
                var ref31 = new ActionReference();
                var idLyr = charIDToTypeID( "Lyr " );
                var idOrdn = charIDToTypeID( "Ordn" );
                var idTrgt = charIDToTypeID( "Trgt" );
                ref31.putEnumerated( idLyr, idOrdn, idTrgt );
            desc93.putReference( idnull, ref31 );
            var idFTcs = charIDToTypeID( "FTcs" );
            var idQCSt = charIDToTypeID( "QCSt" );
            var idQcsa = charIDToTypeID( "Qcsa" );
            desc93.putEnumerated( idFTcs, idQCSt, idQcsa );
            var idOfst = charIDToTypeID( "Ofst" );
                var desc94 = new ActionDescriptor();
                var idHrzn = charIDToTypeID( "Hrzn" );
                var idPxl = charIDToTypeID( "#Pxl" );
                desc94.putUnitDouble( idHrzn, idPxl, 0.000000 );
                var idVrtc = charIDToTypeID( "Vrtc" );
                var idPxl = charIDToTypeID( "#Pxl" );
                desc94.putUnitDouble( idVrtc, idPxl, -0.000000 );
            var idOfst = charIDToTypeID( "Ofst" );
            desc93.putObject( idOfst, idOfst, desc94 );
            var idAngl = charIDToTypeID( "Angl" );
            var idAng = charIDToTypeID( "#Ang" );
            desc93.putUnitDouble( idAngl, idAng, angle );
            var idIntr = charIDToTypeID( "Intr" );
            var idIntp = charIDToTypeID( "Intp" );
            var idBcbc = charIDToTypeID( "Bcbc" );
            desc93.putEnumerated( idIntr, idIntp, idBcbc );
        executeAction( idTrnf, desc93);
    };

    return Watermarker;
}();

var w = new Watermarker;
w.run();
//w.addWatermark('FREE-SHIPPING');















/*
// call the source document
var srcDoc = app.activeDocument;

//get the image with and height
var w = srcDoc.width.value;
var h = srcDoc.height.value;

Object.keys = function(obj){
    var keys = [];
    for (var key in obj) {
        keys.push(key);
    }
    return keys;
};

alert(Object.keys(app.activeDocument).join(' '));


app.activeDocument.selection.selectAll();
app.activeDocument.selection.copy();

// paste into current document
app.activeDocument.paste();

// set the name
srcDoc.activeLayer.name = "watermark"

// set the distance the watermark needs to move
var offsetX = 40;
var offsetY = 20;

//call the function to offset the image
moveActiveLayer(w, h, offsetX, offsetY)

//set opacity
srcDoc.activeLayer.opacity = 40

//flatten the image
srcDoc.flatten();

//save the image
app.activeDocument.close(SaveOptions.SAVECHANGES)

// function MOVE ACTIVE LAYER (layer name, deltaX, deltaY)
// ----------------------------------------------------------------
function moveActiveLayer(imageWidth, imageHeight, dX, dY)
{
    var x = parseFloat(srcDoc.activeLayer.bounds[0])
    var y = parseFloat(srcDoc.activeLayer.bounds[1])
    var x1 = parseFloat(srcDoc.activeLayer.bounds[2])
    var y1 = parseFloat(srcDoc.activeLayer.bounds[3])

    var moveX = (imageWidth - x1) - dX;
    var moveY = (imageHeight- y1) - dY;

    // coords from bottom right

    // Transform layer
    // =======================================================
    var id442 = charIDToTypeID( "Trnf" );
    var desc93 = new ActionDescriptor();
    var id443 = charIDToTypeID( "null" );
    var ref64 = new ActionReference();
    var id444 = charIDToTypeID( "Lyr " );
    var id445 = charIDToTypeID( "Ordn" );
    var id446 = charIDToTypeID( "Trgt" );
    ref64.putEnumerated( id444, id445, id446 );
    desc93.putReference( id443, ref64 );
    var id447 = charIDToTypeID( "FTcs" );
    var id448 = charIDToTypeID( "QCSt" );
    var id449 = charIDToTypeID( "Qcsa" );
    desc93.putEnumerated( id447, id448, id449 );
    var id450 = charIDToTypeID( "Ofst" );
    var desc94 = new ActionDescriptor();
    var id451 = charIDToTypeID( "Hrzn" );
    var id452 = charIDToTypeID( "#Pxl" );
    desc94.putUnitDouble( id451, id452, moveX );
    var id453 = charIDToTypeID( "Vrtc" );
    var id454 = charIDToTypeID( "#Pxl" );
    desc94.putUnitDouble( id453, id454, moveY );
    var id455 = charIDToTypeID( "Ofst" );
    desc93.putObject( id450, id455, desc94 );
    executeAction( id442, desc93, DialogModes.NO );
}*/